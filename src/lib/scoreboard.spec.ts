import { Game } from './game'
import { Scoreboard } from './scoreboard'

describe('Scoreboard', () => {
  const initialGamesData = [{
    id: 2,
    homeTeam: 'Spain',
    homeTeamScore: 10,
    awayTeam: 'Brazil',
    awayTeamScore: 2,
    startDateTime: new Date('2021-10-02')
  },
  {
    id: 3,
    homeTeam: 'Germany',
    homeTeamScore: 2,
    awayTeam: 'France',
    awayTeamScore: 2,
    startDateTime: new Date('2021-10-03')
  },
  {
    id: 4,
    homeTeam: 'Uruguay',
    homeTeamScore: 6,
    awayTeam: 'Italy',
    awayTeamScore: 6,
    startDateTime: new Date('2021-10-04')
  },
  {
    id: 5,
    homeTeam: 'Argentina',
    homeTeamScore: 3,
    awayTeam: 'Australia',
    awayTeamScore: 1,
    startDateTime: new Date('2021-10-05')
  },
  {
    id: 1,
    homeTeam: 'Mexico',
    homeTeamScore: 0,
    awayTeam: 'Canada',
    awayTeamScore: 5,
    startDateTime: new Date('2021-10-01')
  }]
  describe('create', () => {
    it('should create scoreboard with empty game list', () => {
      const scoreboard = Scoreboard.create()
      expect(scoreboard.getGames()).toHaveLength(0)
    })
  })
  describe('getGames', () => {
    it('should return a list of open games sorted by most recently started', () => {
      const initialGames = initialGamesData.map(item => Game.create(item))
      const scoreboard = Scoreboard.create({ initialGames })
      const games = scoreboard.getGames()
      expect(games).toHaveLength(initialGames.length)
      expect(games.map(game => game.id)).toEqual([4, 2, 1, 5, 3])
    })
  })
  describe('startGame', () => {
    it('should start a new game', () => {
      const scoreboard = Scoreboard.create()
      const homeTeam = 'Poland'
      const awayTeam = 'Ukraine'
      const result = scoreboard.startGame({ homeTeam, awayTeam })
      const games = scoreboard.getGames()
      expect(games).toHaveLength(1)
      expect(games[0].id).toBe(result.id)
      expect(games[0].homeTeam).toBe(homeTeam)
      expect(games[0].homeTeamScore).toBe(0)
      expect(games[0].awayTeam).toBe(awayTeam)
      expect(games[0].awayTeamScore).toBe(0)
    })
    it('should create consecutive ids for new games', () => {
      const scoreboard = Scoreboard.create()
      const homeTeam = 'Poland'
      const awayTeam = 'Ukraine'
      const firstResult = scoreboard.startGame({ homeTeam, awayTeam })
      const secondResult = scoreboard.startGame({ homeTeam, awayTeam })
      expect(secondResult.id).toBe(firstResult.id + 1)
    })
  })
  describe('getGameById', () => {
    it('should find a game by id', () => {
      const initialGames = initialGamesData.map(item => Game.create(item))
      const scoreboard = Scoreboard.create({ initialGames })
      const gameId = initialGamesData[0].id
      const game = scoreboard.getGameById(gameId)
      expect(game?.id).toBe(gameId)
    })
  })
  describe('deleteGameById', () => {
    it('should delete an existing game', () => {
      const initialGames = initialGamesData.map(item => Game.create(item))
      const scoreboard = Scoreboard.create({ initialGames })
      const gameIdToDelete = initialGames[0].id
      scoreboard.deleteGameById(gameIdToDelete)
      const game = scoreboard.getGameById(gameIdToDelete)
      expect(game).toBe(undefined)
    })
  })
})
