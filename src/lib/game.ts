export interface GameData {
  id: number
  homeTeam: string
  homeTeamScore: number
  awayTeam: string
  awayTeamScore: number
  startDateTime: Date
}

export interface CreateGameData {
  id: number
  homeTeam: string
  homeTeamScore?: number
  awayTeam: string
  awayTeamScore?: number
  startDateTime?: Date
}

export class Game {
  private readonly _id: number
  private readonly _homeTeam: string
  private _homeTeamScore: number
  private readonly _awayTeam: string
  private _awayTeamScore: number
  private readonly _startDateTime: Date
  get id () { return this._id }
  get homeTeam () { return this._homeTeam }
  get homeTeamScore () { return this._homeTeamScore }
  get awayTeam () { return this._awayTeam }
  get awayTeamScore () { return this._awayTeamScore }
  get startDateTime () { return this._startDateTime }
  get totalScore () { return this._homeTeamScore + this._awayTeamScore }
  private constructor (gameData: GameData) {
    this._id = gameData.id
    this._homeTeam = gameData.homeTeam
    this._homeTeamScore = gameData.homeTeamScore
    this._awayTeam = gameData.awayTeam
    this._awayTeamScore = gameData.awayTeamScore
    this._startDateTime = gameData.startDateTime
  }

  static create ({ id, homeTeam, homeTeamScore = 0, awayTeam, awayTeamScore = 0, startDateTime = new Date() }: CreateGameData): Game {
    return new Game({ id, homeTeam, homeTeamScore, awayTeam, awayTeamScore, startDateTime })
  }

  updateScore ({ homeTeamScore, awayTeamScore }: Pick<GameData, 'awayTeamScore' | 'homeTeamScore'>) {
    this._homeTeamScore = homeTeamScore
    this._awayTeamScore = awayTeamScore
  }
}
