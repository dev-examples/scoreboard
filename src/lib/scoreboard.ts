import { Game, type GameData } from './game'

export class Scoreboard {
  private readonly _games: Game[]
  private constructor ({ initialGames }: { initialGames: Game[] }) {
    this._games = initialGames
  }

  private readonly getNextGameId = () => Math.max(0, ...this._games.map(game => game.id)) + 1

  getGames () {
    return [...this._games].sort((left, right) => {
      const orderByTotalScore = right.totalScore - left.totalScore
      if (orderByTotalScore !== 0) {
        return orderByTotalScore
      }
      const orderByDate = right.startDateTime.valueOf() - left.startDateTime.valueOf()
      return orderByDate
    })
  }

  static create ({ initialGames = [] }: { initialGames?: Game[] } = {}) {
    return new Scoreboard({ initialGames })
  }

  startGame ({ awayTeam, homeTeam }: Pick<GameData, 'homeTeam' | 'awayTeam'>) {
    const game = Game.create({ id: this.getNextGameId(), awayTeam, homeTeam })
    this._games.push(game)
    return { id: game.id }
  }

  deleteGameById (gameId: number) {
    const deleteIndex = this._games.findIndex(game => game.id === gameId)
    this._games.splice(deleteIndex, 1)
  }

  getGameById (gameId: number) {
    return this._games.find(game => game.id === gameId)
  }
}
