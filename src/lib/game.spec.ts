import { Game } from './game'

describe('Game', () => {
  describe('updateScore', () => {
    it('should update score on a game', () => {
      const homeTeam = 'Poland'
      const awayTeam = 'Ukraine'
      const newHomeTeamScore = 2
      const newAwayTeamScore = 4
      const game = Game.create({
        id: 1,
        homeTeam,
        homeTeamScore: 0,
        awayTeam,
        awayTeamScore: 0
      })
      game.updateScore({ homeTeamScore: newHomeTeamScore, awayTeamScore: newAwayTeamScore })
      expect(game.homeTeamScore).toBe(newHomeTeamScore)
      expect(game.awayTeamScore).toBe(newAwayTeamScore)
    })
  })
})
