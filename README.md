# Scoreboard recruitment task

The purpose of this library is to manage a scoreboard of multiple active games/matches between pairs of teams, for example football matches.

## Usage 

The code has to be imported directly, this is not an npm package. Please inspect Typescript sources to learn the interfaces.

### Installing dependencies

`npm install`

### Running tests in watch mode

`npm test -- --watch`

### Running build

`npm run build`

## NOTES

- For a production code I would add more test variations
- UUIDs could be used to avoid generating IDs on Scoreboard entity and could be more robust in parallel processing setup
- I used date object to be more explicit about sorting logic
- I have focused on happy path, there is no exception handling if input to functions is wrong, Typescript should help avoiding such issues
- There is no input data validation e.g. for allowed numbers or characters
- I skipped package build steps, only a basic tsc build task is added
- I started project with react template but decided to go with library approach so there are some commits related to react
- Compatibility with different versions of node.js, npm and OSes is not checked